import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { firestore } from '../../firebase';

const Skills = () => {

    const [skill,setSkill] = useState({
        skillName:"",
        skillParentId:""
    });
    const [loading,setLoading] = useState(false);
    const [parentSkillList, setParentSkillList] = useState([]);
    const [childrenSkillList, setChildrenSkillList] = useState([]);

    const fetchSkillList = async () => {
        try {
            const listParentSkill = [];
            const listChildrenSkill = [];
            await firestore
                .collection('TypeOfSkill')
                .get()
                .then((query) => {
                    query.forEach(doc => {
                        //lấy ra tên phân loại kỹ năng ( type ở trong TypeOfSkill )
                        const { type } = doc.data();

                        listParentSkill.push({
                            id: doc.id,
                            type
                        });
                    })
                });
            await firestore
                .collection('Children_skill')
                .get()
                .then((query) => {
                    query.forEach(doc => {
                        //lấy ra tên phân loại kỹ năng ( type ở trong TypeOfSkill )
                        const { name, parent_skill_id } = doc.data();
                        listChildrenSkill.push({
                            id: doc.id,
                            name,
                            isSelected: false,
                            parent_skill_id
                        });
                    })
                })
            setParentSkillList(listParentSkill);
            setChildrenSkillList(listChildrenSkill)
        } catch (e) {
            alert(e);
        }
    }
    const _getChildrenSkill = (id) => {
        const _list = childrenSkillList;
        const list = [];
        _list.forEach((item) => {
            if (item.parent_skill_id === id) {
                list.push(item)
            }
        })
        return list;
    }
    const handleOnchange = (event) => {
        const { name, value } = event.target;
        setSkill(prevState => ({
            ...prevState,
            [name]: value
        }))
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        await firestore.collection('Children_skill')
        .add({
            name:skill.skillName,
            parent_skill_id:skill.skillParentId
        })
        .then(()=>{
            setLoading(!loading);
            alert('thêm thành công');
        })
    }
    const onDelete = async (id) => {
        if(window.confirm("Bạn có muốn xóa lĩnh vực này?")){
            await firestore.collection('Children_skill').doc(id).delete().then(()=>{
                setLoading(!loading);
                alert('xoa thanh cong');
            });
        }
    }
    useEffect(() => {
        fetchSkillList();
    }, [loading])
    return (
        <div className="wrapper">
            <div className="container-fluid">
                {/* Page-Title */}
                <div className="page-title-box">
                    <div className="row align-items-center">
                        <div className="col-sm-6">
                            <h4 className="page-title">Danh sách kỹ năng</h4>
                        </div>
                        <div className="col-sm-6">
                            <ol className="breadcrumb float-right">
                                <li className="breadcrumb-item"><a href="#">vFreelancer</a></li>
                                <li className="breadcrumb-item active">DS Kỹ năng</li>
                            </ol>
                        </div>
                    </div>
                    {/* end row */}
                </div>
                <div className="row">
                    <div className="col-xl-6">
                        <div className="card m-b-30">
                            <form>
                            <div className="card-body">
                                <h4 className="mt-0 header-title">Thêm kỹ năng mới</h4>
                               <div className="form-group row">
                                    <label htmlFor="example-search-input" className="col-sm-3 col-form-label">Tên lĩnh vực</label>
                                    <div className="col-sm-9">
                                        <input onChange={handleOnchange} className="form-control" type="text" name="skillName"  id="example-search-input" />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label className="col-sm-3 col-form-label">Thuộc kỹ năng</label>
                                    <div className="col-sm-9">
                                        <select className="form-control" name="skillParentId" onChange={handleOnchange}>
                                        <option selected >Selected</option>
                                            {
                                                parentSkillList.map((skill,index)=>(
                                                    <option  key={skill.id} value={skill.id}>{skill.type}</option>
                                                ))
                                            }
                                            
                                            
                                        </select>
                                    </div>
                                </div>
                                <input onClick={handleSubmit} type="submit"  className="btn btn-primary" value="Thêm"/>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div className="col-xl-6">
                        <div className="card m-b-30">
                            <div className="card-body">
                                <h4 className="mt-0 header-title">Danh sách kỹ năng</h4>
                                <div id="accordion">
                                    {
                                        parentSkillList.map(skill => {
                                            return (
                                                <div className="card mb-0">
                                                    <div className="card-header" id="headingOne">
                                                        <h5 className="mb-0 mt-0 font-14">
                                                            <a data-toggle="collapse" data-parent="#accordion" href={`#collapse${skill.id}`} aria-expanded="true" aria-controls="collapseOne" className="text-dark">
                                                                {skill.type}
                                                            </a>
                                                        </h5>
                                                    </div>
                                                    <div id={`collapse${skill.id}`} className="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                                        {
                                                            _getChildrenSkill(skill.id).length > 0 ? (
                                                                _getChildrenSkill(skill.id).map(_skill => (
                                                                    <p className="col-sm-6 col-form-label">+ {_skill.name}
                                                                        <i class="fas fa-window-close" onClick={()=>onDelete(_skill.id)}></i>
                                                                    </p>
                                                                ))
                                                            ) : (
                                                                <p className="col-sm-6 col-form-label">Chưa có kỹ năng nào.</p>
                                                            )
                                                        }
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end row */}
            </div>
            {/* end container-fluid */}
        </div>

    );
}


export default Skills;