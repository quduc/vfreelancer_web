import React, { useEffect, useState } from 'react';
import {
    useParams, Link
} from "react-router-dom";
import { firestore } from '../../firebase';

const Profile = () => {
    const { id } = useParams();
    const [name, setName] = useState('');
    const [address, setAddress] = useState('');
    const [email, setEmail] = useState('');
    const [dateOfBirth, setDateOfBirth] = useState('');
    const [userId, setUserId] = useState('');
    const [skills, setSkills] = useState([]);
    const [job, setJob] = useState([]);
    const [jobApply, setJobApply] = useState([]);
    const fetchUserInformation = async () => {
        let array = [];
        let arrayJobApply = [];
        let skillArray = [];
        await firestore.collection('users')
            .doc(id)
            .get()
            .then(query => {
                const { address, name, email, dateOfBirth, userId, skill } = query.data();
                skill.forEach((s) => {
                    skillArray.push(s.name);
                })
                setName(name);
                setAddress(address)
                setEmail(email);
                setDateOfBirth(dateOfBirth);
                setUserId(userId);
                setSkills(skillArray);
                firestore.collection('posts')
                    .where('idUserPost', '==', userId)
                    .get()
                    .then(_query => {
                        _query.forEach(doc => {
                            const { idUserPost, nameUserPost, jobBudget, jobDeadline, jobDescription, jobImage, jobName,
                                jopPayment, postTime, skills, type, userApplyId } = doc.data();

                            array.push({
                                idUserPost, nameUserPost, jobBudget, jobDeadline, jobDescription, jobImage, jobName,
                                jopPayment, postTime, skills, type, userApplyId
                            })

                        })
                        setJob(array);
                    });
                firestore.collection('posts')
                    .where('userApplyId', '==', userId)
                    .get()
                    .then(__query => {
                        __query.forEach(_doc => {
                            const { idUserPost, nameUserPost, jobBudget, jobDeadline, jobDescription, jobImage, jobName,
                                jopPayment, postTime, skills, type, userApplyId } = _doc.data();
                            arrayJobApply.push({
                                idUserPost, nameUserPost, jobBudget, jobDeadline, jobDescription, jobImage, jobName,
                                jopPayment, postTime, skills, type, userApplyId
                            })
                        });
                        setJobApply(arrayJobApply);
                    })
            });


    }
    useEffect(() => {
        fetchUserInformation();
    }, []);
    return (
        <div className="wrapper">
            <div className="container-fluid">
                {/* Page-Title */}
                <div className="page-title-box">
                    <div className="row align-items-center">
                        <div className="col-sm-6">
                            <h4 className="page-title">Thông tin User</h4>
                        </div>
                        <div className="col-sm-6">
                            <ol className="breadcrumb float-right">
                                <li className="breadcrumb-item"><a href="javascript:void(0);">vFreelancer</a></li>
                                <li className="breadcrumb-item active">Profile</li>
                            </ol>
                        </div>
                    </div>
                    {/* end row */}
                </div>
                <div className="row">

                    <div className="col-xl-12">
                        <div className="card m-b-30">
                            <div className="card-body">
                                <ul className="nav nav-pills nav-justified" role="tablist">
                                    <li className="nav-item waves-effect waves-light">
                                        <a className="nav-link active" data-toggle="tab" href="#home-1" role="tab">
                                            <span className="d-none d-md-block">Thông tin cơ bản</span><span className="d-block d-md-none"><i className="mdi mdi-home-variant h5" /></span>
                                        </a>
                                    </li>
                                    <li className="nav-item waves-effect waves-light">
                                        <a className="nav-link" data-toggle="tab" href="#profile-1" role="tab">
                                            <span className="d-none d-md-block">Kỹ năng</span><span className="d-block d-md-none"><i className="mdi mdi-account h5" /></span>
                                        </a>
                                    </li>
                                    <li className="nav-item waves-effect waves-light">
                                        <a className="nav-link" data-toggle="tab" href="#messages-1" role="tab">
                                            <span className="d-none d-md-block">Job đã đăng</span><span className="d-block d-md-none"><i className="mdi mdi-email h5" /></span>
                                        </a>
                                    </li>
                                    <li className="nav-item waves-effect waves-light">
                                        <a className="nav-link" data-toggle="tab" href="#settings-1" role="tab">
                                            <span className="d-none d-md-block">Job ứng tuyển</span><span className="d-block d-md-none"><i className="mdi mdi-settings h5" /></span>
                                        </a>
                                    </li>
                                </ul>
                                {/* Tab panes */}
                                <div className="tab-content">
                                    <div className="tab-pane active p-3" id="home-1" role="tabpanel">
                                        <div class="col-12">
                                            <div class="card m-b-30">
                                                <div class="card-body">
                                                    <div className="form-group row">
                                                        <label htmlFor="example-text-input" className="col-sm-2 col-form-label">Tên</label>
                                                        <div className="col-sm-10">
                                                            <input className="form-control" disabled type="text" value={name} id="example-text-input" />
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label htmlFor="example-text-input" className="col-sm-2 col-form-label">Email</label>
                                                        <div className="col-sm-10">
                                                            <input className="form-control" disabled type="text" value={email} id="example-text-input" />
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label htmlFor="example-text-input" className="col-sm-2 col-form-label">Địa chỉ</label>
                                                        <div className="col-sm-10">
                                                            <input className="form-control" disabled type="text" value={address} id="example-text-input" />
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label htmlFor="example-text-input" className="col-sm-2 col-form-label">Ngày sinh</label>
                                                        <div className="col-sm-10">
                                                            <input className="form-control" disabled type="text" value={dateOfBirth} id="example-text-input" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="tab-pane p-3" id="profile-1" role="tabpanel">
                                        {
                                            skills.map(skill => (
                                                <p className="col-sm-6 col-form-label">+ {skill}</p>
                                            ))
                                        }
                                    </div>
                                    <div className="tab-pane p-3" id="messages-1" role="tabpanel">
                                        <div className="col-12">
                                            <div className="card m-b-30">
                                                <div className="card-body">
                                                    <table id="datatable" className="table table-bordered dt-responsive nowrap" style={{ borderCollapse: 'collapse', borderSpacing: 0, width: '100%' }}>
                                                        <thead>

                                                            <tr>
                                                                <th>Tên dự án</th>
                                                                <th>Ngân sách</th>
                                                                <th>Tình trạng</th>
                                                                <th>Thời gian</th>
                                                                <th>Hàng động</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {
                                                                job.map(e => {
                                                                    return (
                                                                        <tr>
                                                                            <td>{e.jobName}</td>
                                                                            <td>{e.jobBudget}</td>
                                                                            <td>{e.type}</td>
                                                                            <td>{e.postTime.toDate().toLocaleTimeString('vi-VN')}</td>
                                                                            <td>
                                                                                <div style={{
                                                                                    display: 'flex',
                                                                                    flexDirection: 'row',
                                                                                    justifyContent: 'space-around',
                                                                                }}>
                                                                                    <Link to={`/profile/${e}`} className="btn btn-primary" >Edit</Link>
                                                                                    <Link to="/skills" className="btn btn-danger" >Delete</Link>
                                                                                </div>

                                                                            </td>
                                                                        </tr>
                                                                    )
                                                                })
                                                            }


                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div> {/* end col */}

                                    </div>
                                    <div className="tab-pane p-3" id="settings-1" role="tabpanel">
                                        <div className="col-12">
                                            <div className="card m-b-30">
                                                <div className="card-body">
                                                    <table id="datatable" className="table table-bordered dt-responsive nowrap" style={{ borderCollapse: 'collapse', borderSpacing: 0, width: '100%' }}>
                                                        <thead>

                                                            <tr>
                                                                <th>Tên dự án</th>
                                                                <th>Ngân sách</th>
                                                                <th>Tình trạng</th>
                                                                <th>Nguời đăng</th>
                                                                <th>Thời gian</th>
                                                                <th>Hàng động</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {
                                                                jobApply.map(e => {
                                                                    return (
                                                                        <tr>
                                                                            <td>{e.jobName}</td>
                                                                            <td>{e.jobBudget}</td>
                                                                            <td>{e.type}</td>
                                                                            <td>{e.nameUserPost}</td>
                                                                            <td>{e.postTime.toDate().toLocaleTimeString('vi-VN')}</td>
                                                                            <td>
                                                                                <div style={{
                                                                                    display: 'flex',
                                                                                    flexDirection: 'row',
                                                                                    justifyContent: 'space-around',
                                                                                }}>
                                                                                    <Link to={`/profile/${e}`} className="btn btn-primary" >Edit</Link>
                                                                                    <Link to="/skills" className="btn btn-danger" >Delete</Link>
                                                                                </div>

                                                                            </td>
                                                                        </tr>
                                                                    )
                                                                })
                                                            }


                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div> {/* end col */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            {/* end container-fluid */}
        </div>




    )
}
export default Profile;