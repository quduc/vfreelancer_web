import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { firestore } from '../../firebase';
import { formatDate } from '../../util';
const ListJob = () => {
  const [listJob, setListJob] = useState([]);
  const [keyword, setKeyword] = useState(null);
  const [allJob, setAllJob] = useState([]);
  const [type, setType] = useState([]);
  const [skill, setSkill] = useState([]);
  const fetchListJobs = async () => {
    try {
      let array = [];
      let arraySkill = [];
      await firestore
        .collection('posts')
        .get()
        .then(query => {
          query.forEach(doc => {
            const { jobName, jobDescription, jobBudget, nameUserPost } = doc.data();

            array.push({
              id: doc.id,
              jobName, jobDescription, jobBudget, nameUserPost
            });
          })
        });
      setListJob(array);
      setAllJob(array);
    } catch (e) {
      alert(e);
    }
  }

  useEffect(() => {
    fetchListJobs();
  }, [])
  const handleSearch = (e) => {
    const keyword = e.target.value;
    setListJob(
      allJob.filter(l => (
        l.jobName.toLowerCase().includes(keyword.toLowerCase())
        || l.jobDescription.toLowerCase().includes(keyword.toLowerCase())
        || l.nameUserPost.toLowerCase().includes(keyword.toLowerCase())
      )))

  }

  return (
    <div className="wrapper">
      <div className="container-fluid">
        {/* Page-Title */}
        <div className="page-title-box">
          <div className="row align-items-center">
            <div className="col-sm-6">
              <h4 className="page-title">Danh sách viêc làm</h4>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-right">
                <li className="breadcrumb-item"><a href="javascript:void(0);">vFreelancer</a></li>
                <li className="breadcrumb-item active">DS Job</li>
              </ol>
            </div>
          </div>
          {/* end row */}
        </div>
        <div className="row">
          <div className="col-12">
            <div className="card m-b-30">
              <div className="card-body">
                <div className="form-group row">
                  <h6 className="col-sm-5">DANH SÁCH NGƯỜI DÙNG</h6>
                  <div className="col-sm-4">
                    <input type="text" className="form-control" name="keyword" value={keyword} onChange={handleSearch} placeholder="Search ...." />
                  </div>

                </div>
                <table id="datatable" className="table table-bordered dt-responsive nowrap" style={{ borderCollapse: 'collapse', borderSpacing: 0, width: '100%' }}>
                  <thead>

                    <tr>
                      <th>Tên</th>
                      <th>Mô tả</th>
                      <th>Ngân sách</th>
                      <th>Tên người đăng</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      listJob.map(job => {
                        return (
                          <tr>
                            <td>{job.jobName}</td>
                            <td>{job.jobDescription}</td>
                            <td>{job.jobBudget}</td>
                            <td>{job.nameUserPost}</td>
                            <td>
                              <div style={{
                                display: 'flex',
                                flexDirection: 'row',
                                justifyContent: 'space-around',
                              }}>
                                <Link to="/skills" className="btn btn-danger" >Delete</Link>
                              </div>

                            </td>
                          </tr>
                        )
                      })
                    }


                  </tbody>
                </table>
              </div>
            </div>
          </div> {/* end col */}
        </div> {/* end row */}
      </div>
      {/* end container-fluid */}
    </div>


  );
}


export default ListJob;