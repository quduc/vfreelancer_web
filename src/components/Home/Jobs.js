import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { firestore } from '../../firebase';
import Select from 'react-select'
const Jobs = () => {

    const [formData, setFormData] = useState({
        jobName:'',
        jobBudget:'',
        jobPayment:'',
        jobDeadline:'',
        jobDescription:'',
        skills:[],
        idUserPost:'',
        nameUserPost:''
    });

    const [skillData,setSkillData] = useState([]);
    const [userData,setUserData] = useState([]);
    const [skills, setSkills] = useState([]);
    const [users, setUsers] = useState([]);
    const fetchSkillList = async () => {
        try {
            const array = [];
            const arrayUser = [];
            await firestore
                .collection('users')
                .get()
                .then(query => {
                    query.forEach(doc => {
                        const { userId, name } = doc.data();
                        arrayUser.push({
                            value:userId,
                            label:name
                        })
                    });
                    setUsers(arrayUser);

                })

            await firestore
                .collection('Children_skill')
                .get()
                .then((query) => {
                    query.forEach(doc => {

                        const { name, parent_skill_id } = doc.data();
                        array.push({
                            id:doc.id,
                            value:name,
                            label:name,
                            parent_skill_id
                        });
                    })
                })
            setSkills(array);
        } catch (e) {
            alert(e);
        }
    }

    
    const handleOnchange = (event) => {
        const { name, value } = event.target;
        setFormData(prevState => ({
            ...prevState,
            [name]: value
        }));
        
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        let idUserPost = '';
        let nameUserPost = '';
        
        userData.forEach(user =>{
            idUserPost=user.value;
            nameUserPost=user.label
        } )
        let arraySkill = [];
        skillData.forEach(skill=>arraySkill.push({
            id:skill.id,
            name:skill.value,
            parent_skill_id:skill.parent_skill_id
        }));
        let jobBudget  = formData.jobBudget;
        let jobDeadline = formData.jobDeadline;
        let jobDescription=formData.jobDescription;
        let jobImage = null;
        let jobName = formData.jobName;
        let jobPayment  = formData.jobPayment;
        let postTime = new Date();
        let skill = arraySkill;
        let type="open";

        await firestore.collection('posts')
        .add({
            idUserPost,
            jobBudget,
            jobPayment,
            jobName,
            jobImage,
            jobDescription,
            jobDeadline,
            nameUserPost,
            type,
            skills:skill,
            postTime
        })
        .then(()=>alert('them thanh cong'));

    }

    useEffect(() => {
        fetchSkillList();
    }, [])
    return (
        <div className="wrapper">
            <div className="container-fluid">
                {/* Page-Title */}
                <div className="page-title-box">
                    <div className="row align-items-center">
                        <div className="col-sm-6">
                            <h4 className="page-title">Thêm dự án</h4>
                        </div>
                        <div className="col-sm-6">
                            <ol className="breadcrumb float-right">
                                <li className="breadcrumb-item"><a href="#">vFreelancer</a></li>
                                <li className="breadcrumb-item active">Thêm dự án</li>
                            </ol>
                        </div>
                    </div>
                    {/* end row */}
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="card m-b-30">
                            <div className="card-body">

                                <form>
                                    <div className="form-group row">
                                        <label htmlFor="example-text-input" className="col-sm-2 col-form-label">Tên dự án</label>
                                        <div className="col-sm-10">
                                            <input onChange={handleOnchange} className="form-control" type="text" name="jobName" id="example-text-input" />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="example-text-input" className="col-sm-2 col-form-label">Mô tả</label>
                                        <div className="col-sm-10">
                                            <textarea onChange={handleOnchange} className="form-control" name="jobDescription" rows="15" />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="example-text-input" className="col-sm-2 col-form-label">Ngân sách</label>
                                        <div className="col-sm-10">
                                            <input onChange={handleOnchange} className="form-control" type="text" name="jobBudget" id="example-text-input" />
                                        </div>
                                    </div>
                                    
                                    <div className="form-group row">
                                        <label htmlFor="example-text-input" className="col-sm-2 col-form-label">Thời gian hoàn thành</label>
                                        <div className="col-sm-10">
                                            <input onChange={handleOnchange} className="form-control" type="text" name="jobDeadline" id="example-text-input" />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-2 col-form-label">Hình thức thanh toán</label>
                                        <div className="col-sm-10">
                                            <select className="form-control" name="jobPayment" onChange={handleOnchange}>
                                                <option defaultValue="" >Selected</option>                                            
                                                <option  value="Trả theo giá cố định">Gía cố định</option>
                                                <option  value="Trả theo giờ">Theo giờ</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-2 col-form-label">User</label>
                                        <div className="col-sm-10">
                                        <Select isMulti onChange={setUserData}  value={userData} options={users}/>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="example-text-input" className="col-sm-2 col-form-label">Kỹ năng</label>
                                        <div className="col-sm-10">
                                            <Select isMulti onChange={setSkillData}  value={skillData} options={skills}/>

                                        </div>
                                    </div>
                                    <input onClick={handleSubmit} type="button" className="btn btn-primary" value="Posts" />

                                </form>
                            </div>
                        </div>
                    </div> {/* end col */}
                </div> {/* end row */}
            </div>
            {/* end container-fluid */}
        </div>


    );
}


export default Jobs;