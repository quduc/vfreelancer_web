import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { firestore } from '../../firebase';
import { formatDate } from '../../util';
const ListUser = () => {
  const [listUser, setListUser] = useState([]);
  const [keyword, setKeyword] = useState(null);
  const [allUser, setAllUser] = useState([]);
  const [type, setType] = useState([]);
  const [skill, setSkill] = useState([]);
  const fetchListUser = async () => {
    try {
      let array = [];
      let arraySkill = [];
      await firestore
        .collection('users')
        .get()
        .then(query => {
          query.forEach(doc => {
            const { name, email, created_at, address, skill } = doc.data();

            array.push({
              id: doc.id,
              address,
              name,
              email,
              created_at,
              skill
            });
            skill.forEach(s => {
              arraySkill.push({
                typeID: s.parent_skill_id,

              })
            })
          })
        });
      setListUser(array);
      setAllUser(array);
    } catch (e) {
      alert(e);
    }
  }
  const fetchType = async () => {
    let array = [];
    await firestore
      .collection('TypeOfSkill')
      .get()
      .then(query => {
        query.forEach(doc => {
          const { type } = doc.data();
          array.push({
            type,
            id: doc.id
          })
        });
        setType(array);
      })
  }
  useEffect(() => {
    fetchListUser();
    fetchType();
  }, [])
  const handleSearch = (e) => {
    const keyword = e.target.value;
    setListUser(
      allUser.filter(l => (
        l.name.toLowerCase().includes(keyword.toLowerCase())
        || l.email.toLowerCase().includes(keyword.toLowerCase())
        || l.address.toLowerCase().includes(keyword.toLowerCase())
      )))

  }
  const handleFilter = (e) => {
    const filter = e.target.value;
    setListUser(
      allUser.filter(l => (
        l.name.toLowerCase().includes(filter.toLowerCase())
        || l.nameDriver.toLowerCase().includes(filter.toLowerCase())
        || l.typeBooking.toLowerCase().includes(filter.toLowerCase())
        || l.date.toLowerCase().includes(filter.toLowerCase())
        || l.statusBooking.toLowerCase().includes(filter.toLowerCase())
      )))
  }
  return (
    <div className="wrapper">
      <div className="container-fluid">
        {/* Page-Title */}
        <div className="page-title-box">
          <div className="row align-items-center">
            <div className="col-sm-6">
              <h4 className="page-title">Danh sách người dùng</h4>
            </div>
            <div className="col-sm-6">
              <ol className="breadcrumb float-right">
                <li className="breadcrumb-item"><a href="javascript:void(0);">vFreelancer</a></li>
                <li className="breadcrumb-item active">DS User</li>
              </ol>
            </div>
          </div>
          {/* end row */}
        </div>
        <div className="row">
          <div className="col-12">
            <div className="card m-b-30">
              <div className="card-body">
                <div className="form-group row">
                  <h6 className="col-sm-6">DANH SÁCH NGƯỜI DÙNG</h6>
                  <div className="col-sm-6">
                    <input type="text" className="form-control" name="keyword" value={keyword} onChange={handleSearch} placeholder="Search ...." />
                  </div>
                  

                </div>
                <table id="datatable" className="table table-bordered dt-responsive nowrap" style={{ borderCollapse: 'collapse', borderSpacing: 0, width: '100%' }}>
                  <thead>

                    <tr>
                      <th>Tên</th>
                      <th>Email</th>
                      <th>Địa chỉ</th>
                      <th>Created At</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      listUser.map(user => {
                        return (
                          <tr>
                            <td>{user.name}</td>
                            <td>{user.email}</td>
                            <td>{user.address}</td>
                            <td>{formatDate(user.created_at)}</td>
                            <td>
                              <div style={{
                                display: 'flex',
                                flexDirection: 'row',
                                justifyContent: 'space-around',
                              }}>
                                <Link to={`/profile/${user.id}`} params={user.id} className="btn btn-primary" >Edit</Link>
                                <Link to="/skills" className="btn btn-danger" >Delete</Link>
                              </div>

                            </td>
                          </tr>
                        )
                      })
                    }


                  </tbody>
                </table>
              </div>
            </div>
          </div> {/* end col */}
        </div> {/* end row */}
      </div>
      {/* end container-fluid */}
    </div>


  );
}


export default ListUser;