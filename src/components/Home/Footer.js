import React from 'react';

const Footer = () =>{
        return (
            <footer className="footer">
            © 2021 vFreelancer <span className="d-none d-sm-inline-block"> - All right reserved.</span>.
        </footer>
        );
    
}

export default Footer;