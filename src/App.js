import React, { useContext, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import Login from './components/Login';
import Home from './components/Home/Home';
import Header from './components/Home/Header';
import Footer from './components/Home/Footer';
import { auth } from './firebase';
import { AuthContext } from './provider/AuthProvider';
import PrivatedRoute from './components/PrivatedRoute';
import Profile from './components/Home/Profile';
import Skills from './components/Home/Skills';
import Jobs from './components/Home/Jobs';
import ListJob from './components/Home/ListJob';
const App = () => {

  const {setUser } = useContext(AuthContext);
  function onChange(user) {
    setUser(user);
  }
  // https://firebase.google.com/docs/auth/web/manage-users

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged(onChange)

    // unsubscribe to the listener when unmounting
    return () => unsubscribe()
  }, []);
  const HomeComponent = () => {
    return (
      <div>
        <Header />
        <Home />
        <Footer />
      </div>
    )
  }
  const SkillComponent = () => {
    return (
      <div>
        <Header />
        <Skills />
        <Footer />
      </div>
    )
  }
  const JobComponent = () => {
    return (
      <div>
        <Header />
        <Jobs />
        <Footer />
      </div>
    )
  }
  const ListJobComponent = () => {
    return (
      <div>
        <Header />
        <ListJob />
        <Footer />
      </div>
    )
  }
  const ProfileComponent = () => {
    return (
      <div>
        <Header />
        <Profile />
        <Footer />
      </div>
    )
  }
  return (
    <Router>
      <Switch>
        <PrivatedRoute exact path="/" component={HomeComponent} />
        <PrivatedRoute exact path="/profile/:id" component={ProfileComponent} />
        <PrivatedRoute exact path="/skills" component={SkillComponent} />
        <PrivatedRoute exact path="/add_job" component={JobComponent} />
        <PrivatedRoute exact path="/jobs" component={ListJobComponent} />
        <Route path="/login">
          <Login />
        </Route>
      </Switch>
    </Router>

  );
}

export default App;
