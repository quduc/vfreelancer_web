import moment from 'moment';
export const formatDate = (dateString) => {
    return moment(dateString).format('DD/MM/YYYY');
};
export const formatDateAndTime = (dateString) => {
    return moment(dateString).format('h:mm [ngày] DD/MM');
}
export const formatTime = (dateString) => {
    return moment(dateString).format('h:mm');
}