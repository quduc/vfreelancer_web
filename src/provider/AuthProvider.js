import React , { useState, createContext, useEffect , useContext } from 'react';
import {auth} from '../firebase';
export const AuthContext = createContext();
export const useAuth = () => {
    return useContext(AuthContext);
}
export const AuthProvider = ({children}) => {
    const [user,setUser] = useState(null);
    const [loading,setLoading] = useState(true);
    const login = (email,password) => {
        return auth.signInWithEmailAndPassword(email,password)
    }
    const logout = () => {
        return auth.signOut();
    }
    useEffect(()=>{
        const un = auth.onAuthStateChanged(user=>{
            setUser(user);
            setLoading(false);
        })
        return un;
    },[])
    
    return (
        <AuthContext.Provider 
            value={{
                user,
                setUser,
                login,
                logout
            }}
        >
            {!loading && children}
        </AuthContext.Provider>
    )
}
